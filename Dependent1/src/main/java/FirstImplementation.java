import settings.Description;
import settings.MyOwnInterface;

@Description(description = "Roznica pomiedzy podana liczba i liczba znakow w podanym ciagu znakow")
public class FirstImplementation implements MyOwnInterface {
    @Override
    public String methodToImplement(int numberOfTimes, String givenString) {
        int liczbaZnakow = givenString.length();
        int roznica = numberOfTimes - liczbaZnakow;

        return String.valueOf(roznica);
    }
}
