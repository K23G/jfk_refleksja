import settings.Description;
import settings.MyOwnInterface;

@Description(description = "Powtarzanie podanego ciągu znaków podaną liczbę razy")
public class SecondImplementation implements MyOwnInterface {

    @Override
    public String methodToImplement(int numberOfTimes, String givenString) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(givenString);
        for (int i=0; i<numberOfTimes; i++) {
            stringBuilder.append(" "+givenString);
        }
        return String.valueOf(stringBuilder);
    }
}
