package app;

import controller.AppController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import settings.MyOwnInterface;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class MethodFinder {

    private static final String EXTENSION = ".jar";
    /*Lista elementów*/
    private ObservableList<MyOwnInterface> myInterfaceObservableList = FXCollections.observableArrayList();

    /*PRZESZUKIWANIE FOLDERU W CELU ZNALEZIENIA PLIKÓW JAR*/
    private List<String> scanForJarFiles(String path) {
        File pathToScan = new File(path);
        List<String> pathsToJarFiles = new ArrayList<>();

        List<File> files = new ArrayList<>(Arrays.asList(Objects.requireNonNull(pathToScan.listFiles())));

        files.stream()
                .filter(File::isFile)
                .filter(file -> file.getName().endsWith(EXTENSION))
                .forEach(file -> {
                    String pathToJarFile = file.getAbsolutePath();
                    pathsToJarFiles.add(pathToJarFile);
                });
        return pathsToJarFiles;
    }

    /*SKANOWANIE W POSZUKIWANIU KLAS, KTÓRE IMPLEMENTUJA INTERFACE*/
    private void scanForImplemented(String jarPath) throws Exception {
        JarFile jarFile = null;
        try {
            jarFile = new JarFile(jarPath);
            Enumeration<JarEntry> entries = jarFile.entries();

            URL[] urls = {new URL("jar:file:" + jarPath + "!/")};
            URLClassLoader cl = URLClassLoader.newInstance(urls);

            while (entries.hasMoreElements()) {
                JarEntry je = entries.nextElement();
                if (je.isDirectory() || !je.getName().endsWith(".class")) {
                    continue;
                }

                String className = je.getName().substring(0, je.getName().length() - 6);
                className = className.replace('/', '.');
                try {
                    Class<?> c = cl.loadClass(className);

                    MyOwnInterface callable = (MyOwnInterface) c.newInstance();

                    if (MyOwnInterface.class.isAssignableFrom(c)) {
                        callable = (MyOwnInterface) c.newInstance();

                        if (null == callable)
                            throw new Exception();
                        /*return callable;*/
                        myInterfaceObservableList.add(callable);
                    }
                } catch (ClassNotFoundException exp) {
                    continue;
                }

            }
        } catch (IOException exp) {
            exp.printStackTrace();
        } finally {
            if (null != jarFile)
                jarFile.close();
        }

        /*return null;*/
    }

    /*DODAWANIE PLIKÓW JAR DO LISTY*/
    private void addFromPathToObservableList(String path) {
        List<String> pathsToJarFiles = scanForJarFiles(path);

        if (pathsToJarFiles.size() == 0)
            AppController.makeAlert(Alert.AlertType.INFORMATION, "Warning", "Brak plików", "Podaj katalog w którym znajdują się pliki jar...");

        for (String pathToFile : pathsToJarFiles) {
            try {
                scanForImplemented(pathToFile);
            } catch (Exception e) {
                AppController.makeAlert(Alert.AlertType.ERROR, "BLAD", "Brak instancji w podanym pliku jar", "W podanym pliku jar " + pathToFile + " nie ma klas implementujących interface");
            }
        }
    }

    public MethodFinder() {

    }

    public ObservableList<MyOwnInterface> getObservableListForGivenPath(String path) {
        addFromPathToObservableList(path);

        return myInterfaceObservableList;
    }
}
