package settings;

public interface MyOwnInterface {
    String methodToImplement(int numberOfTimes, String givenString);
}
