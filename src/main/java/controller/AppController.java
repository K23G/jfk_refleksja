package controller;

import app.MethodFinder;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import settings.Description;
import settings.MyOwnInterface;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

public class AppController implements Initializable {
    public AnchorPane mainPane;
    public TextField selectedCatalog;
    public TableView<MyOwnInterface> tableWithMethods;
    public TextField arg1Field;
    public TextField arg2Field;
    public TextArea resultField;
    public Button closeButton;
    public Button runButton;
    public Button openButton;
    public TableColumn<MyOwnInterface, Integer> idColumn;
    public TableColumn<MyOwnInterface, String> nameColumn;
    public TableColumn<MyOwnInterface, String> metadataColumn;

    /*Lista elementów*/
    private ObservableList<MyOwnInterface> myInterfaceObservableList = FXCollections.observableArrayList();

    /*INICJALIZACJA KONTROLLERA FX*/
    public void initialize(URL location, ResourceBundle resources) {
        openButton.setOnAction(e -> {
            arg1Field.clear();
            selectedCatalog.clear();
            arg2Field.clear();
            resultField.clear();
            myInterfaceObservableList.clear();
            getDirectory();
        });
        closeButton.setOnAction(e -> System.exit(1));
        tableViewProperties();
        arg1Field.setOnKeyReleased(k -> {
            if (!arg1Field.getCharacters().chars().allMatch(Character::isDigit)) {
                makeAlert(Alert.AlertType.WARNING, "Blad", "Zly format danych", "Wprowadzono niepoprawny argument, proszę wprowadzić liczbę.");
                arg1Field.clear();
            }
        });
        runButton.setOnAction(e -> runMethod());
        setFactories();
    }

    private void runMethod() {
        boolean isFirstArgumentInteger = false;
        boolean isSecondArgumentPresent = false;
        boolean isClassSelected = false;

        isFirstArgumentInteger = arg1Field.getCharacters().chars().allMatch(Character::isDigit) && (arg1Field.getCharacters().length() != 0);
        isSecondArgumentPresent = arg2Field.getText().length() != 0;
        isClassSelected = !tableWithMethods.getSelectionModel().isEmpty();

        if (isFirstArgumentInteger && isSecondArgumentPresent && isClassSelected) {
            MyOwnInterface item = tableWithMethods.getSelectionModel().getSelectedItem();
            resultField.setText(item.methodToImplement(Integer.parseInt(arg1Field.getText()), arg2Field.getText()));
        } else {
            makeAlert(Alert.AlertType.WARNING, "Warning", "Nie mozna wykonać działania", "Proszę o wprowadzenie poprawnych danych w pola argumentu oraz wybranie klasy, z której ma zostać wywołana metoda");
        }
    }

    private void getDirectory() {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        Stage stg = (Stage) mainPane.getScene().getWindow();
        File file = directoryChooser.showDialog(stg);

        if (file != null) {
            String pathForSelectedCatalog = file.getAbsolutePath();
            selectedCatalog.setText(pathForSelectedCatalog);
            MethodFinder methodFinder = new MethodFinder();
            myInterfaceObservableList.setAll(methodFinder.getObservableListForGivenPath(pathForSelectedCatalog));
        } else {
            makeAlert(Alert.AlertType.INFORMATION, "Blad", "Nie wskazano folderu", "Prosze wskazać poprawny folder...");
        }
    }

    private void tableViewProperties() {
        tableWithMethods.setItems(myInterfaceObservableList);

        tableWithMethods.setOnMouseClicked(event -> {
            if (event.getClickCount() >= 2 && tableWithMethods.getSelectionModel().getSelectedItem() != null) {
                String description = tableWithMethods.getSelectionModel().getSelectedItem().getClass().isAnnotationPresent(Description.class) ? tableWithMethods.getSelectionModel().getSelectedItem().getClass().getAnnotation(Description.class).description() : "Brak opisu";
                makeAlert(Alert.AlertType.INFORMATION, "Klasa wybrana", description, "Prosze wpisać argumenty do metody poniżej.");
            }
        });
    }

    private void setFactories() {
        idColumn.setCellValueFactory(element -> {
            return new SimpleIntegerProperty(myInterfaceObservableList.indexOf(element.getValue()) + 1).asObject();
        });
        nameColumn.setCellValueFactory(element -> new SimpleStringProperty(element.getValue().getClass().getSimpleName()));
        metadataColumn.setCellValueFactory(element -> {
            String descriptionString = element.getValue().getClass().isAnnotationPresent(Description.class) ? element.getValue().getClass().getAnnotation(Description.class).description() : "Brak opisu";
            String arguments = element.getValue().getClass().getMethods()[0].getParameterTypes()[0].getSimpleName() + ", " + element.getValue().getClass().getMethods()[0].getParameterTypes()[1].getSimpleName();
            String returningType = element.getValue().getClass().getMethods()[0].getReturnType().getSimpleName();

            String metadata =
                    "Opis: " + descriptionString + "\n"
                            + "Typy parametrów: " + arguments + "\n"
                            + "Zwracany typ: " + returningType;
            return new SimpleStringProperty(metadata);
        });
        metadataColumn.setCellFactory(element -> {
            TableCell<MyOwnInterface, String> cell = new TableCell<>();
            Text text = new Text();
            cell.setGraphic(text);
            cell.setPrefHeight(Control.USE_COMPUTED_SIZE);
            text.wrappingWidthProperty().bind(metadataColumn.widthProperty());
            text.textProperty().bind(cell.itemProperty());
            return cell;
        });
        resultField.setWrapText(true);
    }

    public static void makeAlert(Alert.AlertType type, String title, String header, String context) {
        Alert alert = new Alert(type, context);
        alert.setHeaderText(header);
        alert.setTitle(title);
        alert.showAndWait();
    }
}
