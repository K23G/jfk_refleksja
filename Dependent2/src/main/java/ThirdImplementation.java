import settings.Description;
import settings.MyOwnInterface;

import java.util.ArrayList;
import java.util.List;

@Description(description = "Zwraca jeden z 10 wybranych cytatów. Jeżeli wpiszemy 'Tak' jako drugi argument wypisuje również autora")
public class ThirdImplementation implements MyOwnInterface {
    @Override
    public String methodToImplement(int numberOfTimes, String givenString) {
        List<String> listaCytatów = new ArrayList<>();
        List<String> listaAutorów = new ArrayList<>();

        listaCytatów.add(0, "To, że milczę, nie znaczy, że nie mam nic do powiedzenia.");
        listaAutorów.add(0, "Jonathan Carroll");
        listaCytatów.add(1, "Lepiej zaliczać się do niektórych, niż do wszystkich.");
        listaAutorów.add(1, "Andrzej Sapkowski");
        listaCytatów.add(2, "Czytanie książek to najpiękniejsza zabawa, jaką sobie ludzkość wymyśliła");
        listaAutorów.add(2, "Wisława Szymborska");
        listaCytatów.add(3, "Dobrze widzi się tylko sercem. Najważniejsze jest niewidoczne dla oczu");
        listaAutorów.add(3, "Antoine de Saint-Exupéry");
        listaCytatów.add(4, "Książki są lustrem: widzisz w nich tylko to co, już masz w sobie.");
        listaAutorów.add(4, "Carlos Ruiz Zafón");
        listaCytatów.add(5, "W chwili, kiedy zastanawiasz się czy kogoś kochasz, przestałeś go już kochać na zawsze.");
        listaAutorów.add(5, "Carlos Ruiz Zafón");
        listaCytatów.add(6, "Kocha się za nic. Nie istnieje żaden powód do miłości.");
        listaAutorów.add(6, "Paulo Coelho");
        listaCytatów.add(7, "Kto czyta książki, żyje podwójnie.\n");
        listaAutorów.add(7, "Umberto Eco");
        listaCytatów.add(8, "(...) ludzie mają wrodzony talent do wybierania właśnie tego, co dla nich najgorsze.");
        listaAutorów.add(8, " J.K. Rowling");
        listaCytatów.add(9, "Istniejemy póki ktoś o nas pamięta.");
        listaAutorów.add(9, "Carlos Ruiz Zafón");
        listaCytatów.add(10, "Miałem zły dzień. Tydzień. Miesiąc. Rok. Życie. Cholera jasna.");
        listaAutorów.add(10, "Charles Bukowski");

        if (givenString.toUpperCase().equals("TAK"))
            return listaCytatów.get(numberOfTimes % 10) + " Autor: " + listaAutorów.get(numberOfTimes % 10);
        else return listaCytatów.get(numberOfTimes % 10);
    }
}
