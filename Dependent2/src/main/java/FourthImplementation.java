import settings.Description;
import settings.MyOwnInterface;

@Description(description = "Sprawdzanie czy podana liczba jest taka sama jak podany ciąg znaków")
public class FourthImplementation implements MyOwnInterface {
    @Override
    public String methodToImplement(int numberOfTimes, String givenString) {
        return givenString.equals(String.valueOf(numberOfTimes)) ? "Prawda" : "Fałsz";
    }
}
